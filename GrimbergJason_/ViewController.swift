//
//  ViewController.swift
//  GrimbergJason_
//
//  Created by Jason Grimberg on 6/15/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    // Create an empty array of type UITextField
    //var allTextFields = [UITextField]()
    var myArray = [String]()
    
    // Outlets for all text fields
    @IBOutlet weak var txtField1: UITextField!
    @IBOutlet weak var txtField2: UITextField!
    @IBOutlet weak var txtField3: UITextField!
    @IBOutlet weak var txtField4: UITextField!
    @IBOutlet weak var txtField5: UITextField!
    @IBOutlet weak var txtField6: UITextField!
    @IBOutlet weak var lblField: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Prepare all the data to go to the next storyboard
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Check if any text fields are empty
        if (txtField1.text?.isEmpty)! || (txtField2.text?.isEmpty)! || (txtField3.text?.isEmpty)! || (txtField4.text?.isEmpty)! || (txtField5.text?.isEmpty)! || (txtField6.text?.isEmpty)! {
            
            // Set the alert message
            let alert = UIAlertController(title: "Warning", message: "You must fill out all input fields", preferredStyle: UIAlertController.Style.alert)
            
            // Set the OK button
            let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            
            // Add the OK button to the alert
            alert.addAction(okButton)
            
            // Present the alert to the user
            present(alert, animated: true, completion: nil)
        }
        else {
            // Let the destination know what data we are passing through
            let destination = segue.destination as? SecondViewController
            
            // Add all text fields to array
            myArray = ["\(txtField1.text!)", "\(txtField2.text!)", "\(txtField3.text!)", "\(txtField4.text!)", "\(txtField5.text!)", "\(txtField6.text!)"]
            
            // Send over the array of text to the new array
            destination?.newArray = myArray
            
            // Set all labels to the text boxes in the second view
            destination?.lbl1 = txtField1.text
            destination?.lbl2 = txtField2.text
            destination?.lbl3 = txtField3.text
            destination?.lbl4 = txtField4.text
            destination?.lbl5 = txtField5.text
            destination?.lbl6 = txtField6.text
            
        }
    }
    @IBAction func unwindToRoot(segue: UIStoryboardSegue) {
        // Let the source be the segue from the second view controller
        let source = segue.source as! SecondViewController
        
        lblField.text = source.newSentence
    }
}

