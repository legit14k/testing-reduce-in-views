//
//  SecondViewController.swift
//  GrimbergJason_
//
//  Created by Jason Grimberg on 6/15/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var newArray = [String]()
    
    // Set labels
    var lbl1: String?
    var lbl2: String?
    var lbl3: String?
    var lbl4: String?
    var lbl5: String?
    var lbl6: String?
    var newSentence: String?
    
    // Set variables to labels
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label6: UILabel!
    
    @IBAction func Reduce(_ sender: Any) {
        let newString = newArray.reduce(""){ (result, a) -> String in return result + " \(String(a))"
            }
        
        // Set the array to the new sentence
        newSentence = newString
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set all label texts to the incoming data
        label1.text = lbl1
        label2.text = lbl2
        label3.text = lbl3
        label4.text = lbl4
        label5.text = lbl5
        label6.text = lbl6
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Cancel button - no information returned
    @IBAction func goBack(_ sender: UIButton) {
        //newArray.removeAll()
        dismiss(animated: true, completion: nil)
    }


}
